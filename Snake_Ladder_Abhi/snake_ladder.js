//--start position left:355 top:575/////////////
// store value after rolling dice
// move will store which player has just moved
let move;
let diceValue;

// Roll dice
const max = 6;
let flag = 0;
if(flag==0){
    document.getElementById('id1').innerHTML="Player1's Turn";
    flag=1;
}
function rollDice(){
    let num = Math.ceil(Math.random()*max);
    diceValue = num;
    document.getElementById('printDiceDigit').innerHTML = "point : " + num;
    var x = document.getElementById('id1');
    if(flag==0){
    document.getElementById('id1').innerHTML="Player1's Turn";
    move = 2;
    flag=1;
}
else{
    document.getElementById('id1').innerHTML="Player2's Turn";
    move = 1;
    flag=0;
}
}
/***************************** LOGIC ***************************/
// ladder and snake arrays contains all snakes and ladders.... start and end positions
let ladder = [[3,51],[6,27],[20,70],[36,55],[63,95],[68,98]];
let snake = [[34,1],[25,5],[47,19],[65,52],[87,57],[91,61],[99,69]];

// initially both token are closed... 
let p1_close = 1;
let p2_close = 1;

// initially top and left set to center 1st block...
let topPos=575;
let leftPos=355;

//initially both players are at 1st row...
let player1_row = 1;
let player2_row = 1;

// initially blockNo of both player is zero .. range of blockNo [1-100]
let player1_block = 0;
let player2_block = 0;

//to store left and top pos of players...
let player1_left_position;
let player1_top_position;
let player2_left_position;
let player2_top_position;

// p1, p2--- objects... to change the positions of player1 & player2...
let p1 = document.getElementById('player1');
let p2 = document.getElementById('player2');


// var declared for snake and ladder logic.......
let p1_ladder_new_block;
let p2_ladder_new_block;
let p1_snake_new_block;
let p2_snake_new_block;
let p1lp,p1tp,p2lp,p2tp;
let p1lf=0;
let p1sf=0; 
let p2lf=0; 
let p2sf=0;


// -----------------Main Function-----------------
function main(){

        // 2nd condn of this if....if (player1_block + diceValue)>100.. don't change the pos of that token
        if(move==1 && (player1_block+diceValue)<=100){
            // check if token is open....
            if(p1_close==0){
                // stackoverflow code help
                // to store current left and top position 
                player1_left_position = parseInt(p1.offsetLeft,10) || 0;
                player1_top_position = parseInt(p1.offsetTop,10) || 0;
            }
            //token will open ... when diceValue == 1...
            if(diceValue==1 && p1_close==1){
                p1_close=0;
                player1_left_position = leftPos;
                player1_top_position = topPos;
            }
            if(p1_close==0){
                player1_block += diceValue;

                // Snake & Ladder Logic-----------
                //Ladder-------------
                for(let i=0;i<ladder.length;i++){
                    if(player1_block == ladder[i][0]){
                        p1lf=1;
                        p1_ladder_new_block = ladder[i][1];
                        break;
                    }
                }
                if(p1lf==1){
                    let temp1 = p1_ladder_new_block % 10;
                    let temp2 = Math.floor(p1_ladder_new_block/10);
                    p1lp = leftPos + (temp1)*60;
                    p1tp = topPos - (temp2)*60;
                    if(temp1==0){
                        p1lp = leftPos + 10*60;
                        p1tp = topPos - (temp2-1)*60;
                    }
                    
                    // Update player1's block and row no...
                    player1_block = p1_ladder_new_block;
                    player1_row = Math.ceil(p1_ladder_new_block/10);
                }

                // Snake----------------
                // top value will increase in case of snake....
                for(let i=0;i<snake.length;i++){
                    if(player1_block == snake[i][0]){
                        p1sf=1;
                        p1_snake_new_block = snake[i][1];
                        break;
                    }
                }

                if(p1sf==1){
                    let temp1 = p1_snake_new_block % 10;
                    let temp2 = Math.floor(p1_snake_new_block/10);
                    p1lp = leftPos + (temp1)*60;
                    p1tp = topPos - (temp2)*60;
                    if(temp1==0){
                        p1lp = leftPos + 10*60;
                        p1tp = topPos - (temp2-1)*60;
                    }

                    // Update player1's block and row number...
                    player1_block = p1_snake_new_block;
                    player1_row = Math.ceil(p1_snake_new_block/10);
                }

                if(p1lf!=1 && p1sf!=1){
                if(player1_block <= 10*player1_row){
                    // it means player1 will move only in horizontal direction
                    // if row_no is even it means right to left
                        player1_left_position += 60*(diceValue);
                    
                }
                else{
                    // set player1_block to its previous pos
                    player1_block -= diceValue;
                    //player1 will move in both direction horizonal and vertical
                    let temp = (player1_block + diceValue)%10;
                    player1_left_position = leftPos + (temp*60);
                    player1_top_position -= 60;
                    
                    player1_row += 1;
                    // update player1_block
                    player1_block += diceValue;
                }
            }

            //------- PLAYER 1 break condition -----
            if(p1lf==1 || p1sf==1){
                p1.style.left = p1lp+'px';
                p1.style.top = p1tp+'px';
                p1lf=0;
                p1sf=0;
            }
            else{
                if(player1_block <= 100){

                    p1.style.left = player1_left_position+'px';
                    p1.style.top = player1_top_position+'px';
                }
                if(player1_block==100){
                    window.alert("Player1 has won the game...")
                }
            }
            //------- Snake Logic -----------
            
        }}
//-------------Player 2 move
else if(move==2 && (player2_block + diceValue)<=100){
    // token is already open
    if(p2_close==0){
        player2_left_position = parseInt(p2.offsetLeft,10) || 0;
        player2_top_position = parseInt(p2.offsetTop,10) || 0;
    }
    //
    if(diceValue==1 && p2_close==1){
        p2_close=0;
        player2_left_position = leftPos;
        player2_top_position = topPos;
    }
    
    if(p2_close==0){
        player2_block += diceValue;
         // Snake & Ladder Logic-----------
                //Ladder-------------
                for(let i=0;i<ladder.length;i++){
                    if(player2_block == ladder[i][0]){
                        p2lf=1;
                        p2_ladder_new_block = ladder[i][1];
                        break;
                    }
                }
                if(p2lf==1){
                    let temp1 = p2_ladder_new_block % 10;
                    let temp2 = Math.floor(p2_ladder_new_block/10);
                    p2lp = leftPos + (temp1)*60;
                    p2tp = topPos - (temp2)*60;
                    if(temp1==0){
                        p2lp = leftPos + 10*60;
                        p2tp = topPos - (temp2-1)*60;
                    }
                    
                    // Update player1's block and row no...
                    player2_block = p2_ladder_new_block;
                    player2_row = Math.ceil(p2_ladder_new_block/10);
                }

                // Snake----------------
                // top value will increase in case of snake....
                for(let i=0;i<snake.length;i++){
                    if(player2_block == snake[i][0]){
                        p2sf=1;
                        p2_snake_new_block = snake[i][1];
                        break;
                    }
                }

                if(p2sf==1){
                    let temp1 = p2_snake_new_block % 10;
                    let temp2 = Math.floor(p2_snake_new_block/10);
                    p2lp = leftPos + (temp1)*60;
                    p2tp = topPos - (temp2)*60;
                    if(temp1==0){
                        p2lp = leftPos + 10*60;
                        p2tp = topPos - (temp2-1)*60;
                    }

                    // Update player1's block and row number...
                    player2_block = p2_snake_new_block;
                    player2_row = Math.ceil(p2_snake_new_block/10);
                }

                if(p2lf!=1 && p2sf!=1){
        if(player2_block <= 10*player2_row){
            // it means player2 will move only in horizontal direction
            // if row_no is even it means right to left
                player2_left_position += 60*(diceValue);

        }
        else{
            //player2 will move in both direction horizonal and vertical
            // set player2_block one step before to do some calculaion
            player2_block -= diceValue;
            let temp = (player2_block + diceValue)%10;
            player2_left_position = leftPos + (temp*60);
            player2_top_position -= 60;
            
            player2_row += 1;
            // update player1_block
            player2_block += diceValue;
        }}
    }
    // without px its not working .... check???
    //

    if(p2lf==1 || p2sf==1){
        p2.style.left = p2lp+'px';
        p2.style.top = p2tp+'px';
        p2lf=0;
        p2sf=0;
    }
    else{
    if(player2_block <= 100){
        p2.style.left = player2_left_position+'px';
        p2.style.top = player2_top_position+'px';
    }
    if(player2_block == 100){
        window.alert("player2 has won the game");
    }}

} 

}
