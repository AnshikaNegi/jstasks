var gameLevel = 1;
		var remainingTime = 20;
		var image = document.getElementById("placeImage");
		var baseString = "";
		var inputCharArray = [];
		var interval;

		class Place{
			constructor(name,photo){
				this.name = name;
				this.photo = photo;
			}
		}

		var images = [];
		images.push(new Place("TAJMAHAL","1-tajmahal.jpg"));
		images.push(new Place("REDFORT","2-redfort.jpg"));
		images.push(new Place("GOLDENTEMPLE","3-golden-temple.jpg"));
		images.push(new Place("LOTUSTEMPLE","4-lotus-temple.jpg"));
		images.push(new Place("EIFFELTOWER","5-eiffeltower.jpg"));
		images.push(new Place("JANTARMANTAR","6-jantar-mantar.jpg"));
		images.push(new Place("JAMAMASZID","7-jama-maszid.jpg"));
		images.push(new Place("AKSHARDHAM","8-akshardham.jpg"));
		images.push(new Place("HARIDWAR","9-haridwar.jpg"));
		images.push(new Place("LONDON","10-london.jpg"));
		

		function playGame(){
			runLevel(gameLevel);
			document.getElementById("placeImagePortion").style.display = "block";
			document.getElementById("start-game-button").style.display = "none";
		}

		function runLevel(gameLevel){
		
			remainingTime = 20;
			showImage(gameLevel);
			baseString = printBlankSpace(gameLevel);
			trackUserInput = getBlankResultString(gameLevel);
			
			if(gameLevel <= 10){
				interval = setInterval(playTimer,1000);
				playTimer();
				document.getElementById("textTimeRemaining").style.display = "block";
			}

			function showImage(gameLevel){
				placeImage.src = "images/"+ images[window.gameLevel-1].photo;
			}
			function playTimer(){
				if(remainingTime > 0){
					document.getElementById("textTimeRemaining").innerHTML = remainingTime;
					remainingTime = remainingTime - 1;
				}
				else if(remainingTime !=0 && window.gameLevel <= 10){
					isStringMatchWithResult();
				}
				else if(remainingTime == 0){
					document.getElementById("overlay-gameover").style.display = "block";
					keysEnabled(inputCharArray);
					clearInterval(window.interval);

					if(window.gameLevel <= 10){
						document.getElementById("textLevelInformation").innerHTML = window.gameLevel;
						document.getElementById("textTimeRemaining").innerHTML = remainingTime;
					}
					if(window.gameLevel > 10){
						document.getElementById("textTimeRemaining").style.display = "none";
					}
				}
			}
		}

		function keysEnabled(inputCharArray){
			for(let i=0;i<inputCharArray.length;i++){
				console.log(inputCharArray[i]);
				document.getElementById(inputCharArray[i]).disabled = false;
			}
			inputCharArray = [];
		}
		function getChar(char){
			char.disabled = true;
			processInputCharacter(char);
			inputCharArray.push(char.value);
			isStringMatchWithResult();
		}

		function isStringMatchWithResult(){
			if(trackUserInput == images[gameLevel-1].name){
				console.log("correct");
				gameLevel = gameLevel + 1;
				if(gameLevel <= 10){
					keysEnabled(inputCharArray);
					document.getElementById("textLevelInformation").innerHTML = gameLevel;
					clearInterval(interval);
					runLevel(gameLevel);
				}
				else{
					clearInterval(interval);
					gameCompleted();
				}
			}
		}

		function processInputCharacter(char){
			const placeName = images[gameLevel-1].name;
			const placeLength = placeName.length;
			for(let i=0 ; i<placeLength; i++){
				if(placeName[i] == char.value){
					baseString = setCharAt(baseString,2*i,char.value);
					trackUserInput = setCharAt(trackUserInput,i,char.value);	
				}
			}
			document.getElementById("userInputText").innerHTML = baseString;
		}

		function printBlankSpace(gameLevel){
			var result = "";
			for(var i=0 ; i<images[gameLevel-1].name.length ; i++){
				result = result+ "_ ";
			}
			document.getElementById("userInputText").innerHTML = result;
			return result;
		}
		function getBlankResultString(gameLevel){
			var result2 = "";
			for(var i=0 ; i<images[gameLevel-1].name.length ; i++){
				result2 = result2 + "_";
			}
			return result2;
		}

		function setCharAt(str,index,chr) {
    		if(index > str.length-1) return str;
    		return str.substr(0,index) + chr + str.substr(index+1);
		}

		function retryLevel(){
			document.getElementById("overlay-gameover").style.display = "none";
			runLevel(gameLevel);
		}

		function restartLevel(){
			document.getElementById("overlay-gameover").style.display = "none";
			gameLevel = 1;
			document.getElementById("textLevelInformation").innerHTML = gameLevel;
			runLevel(gameLevel);
		}

		function gameCompleted(){
			document.getElementById("overlay-gamecompleted").style.display = "block";
		}