var dice=document.getElementById("dice");
var star=document.getElementById("start");
var display=document.getElementById("display");
var turn,face,sync=0,syncm=0;
var red=[],green=[],blue=[],yellow=[];
var data=["red","green","yellow","blue"];
// var k=[2,6,6,5,4,6,3];
function tracker(left,top)
{
    this.left=left;
    this.top=top;
}
var t=[new tracker(40,240),new tracker(80,240),new tracker(120,240),new tracker(160,240),new tracker(200,240),
    new tracker(240,200),new tracker(240,160),new tracker(240,120),new tracker(240,80),new tracker(240,40),new tracker(240,0),new tracker(280,0),new tracker(320,0),
    new tracker(320,40),new tracker(320,80),new tracker(320,120),new tracker(320,160),new tracker(320,200),
    new tracker(360,240),new tracker(400,240),new tracker(440,240),new tracker(480,240),new tracker(520,240),new tracker(560,240),new tracker(560,280),new tracker(560,320),
    new tracker(520,320),new tracker(480,320),new tracker(440,320),new tracker(400,320),new tracker(360,320),
    new tracker(320,360),new tracker(320,400),new tracker(320,440),new tracker(320,480),new tracker(320,520),new tracker(320,560),new tracker(280,560),new tracker(240,560),
    new tracker(240,520),new tracker(240,480),new tracker(240,440),new tracker(240,400),new tracker(240,360),
    new tracker(200,320),new tracker(160,320),new tracker(120,320),new tracker(80,320),new tracker(40,320),new tracker(0,320),new tracker(0,280),
    new tracker(0,240)];
var tr=[new tracker(40,280),new tracker(80,280),new tracker(120,280),new tracker(160,280),new tracker(200,280),new tracker(220,280)];
var tg=[new tracker(280,40),new tracker(280,80),new tracker(280,120),new tracker(280,160),new tracker(280,200),new tracker(280,220)];
var tb=[new tracker(280,520),new tracker(280,480),new tracker(280,440),new tracker(280,400),new tracker(280,360),new tracker(280,320)];
var ty=[new tracker(520,280),new tracker(480,280),new tracker(440,280),new tracker(400,280),new tracker(360,280),new tracker(320,280)];
function check(color)
{
    var l=0;
    switch(color)
    {

        case "red":
            for(let i=0;i<4;i++)
                if(red[i].status==1)
                  { 
                    return 1;
                  }
            return 0;
        case "green":
            for(let i=0;i<4;i++)
                if(green[i].status==1)
                  { l=1;

                    return 1;
                  }
            return 0;
        case "yellow":
            for(let i=0;i<4;i++)
                if(yellow[i].status==1)
                  { l=1;
                    return 1;
                  }
            return 0;
        case "blue":
            for(let i=0;i<4;i++)
                if(blue[i].status==1)
                  { l=1;
                    return 1;
                  }
            return 0;
    }
}

function obj(color,id,track)
{
    this.id=id;
    this.color=color;
    this.init=track;
    this.track=0;
    this.status=0;
    this.t=[];
    for(let i=0;i<=50;i++)
      this.t.push(t[(this.init+i)%52]);   
      switch(this.color)
      {
          case "red":
            for(let i=0;i<=6;i++)
            this.t.push(tr[i]);
            break;
        case "green":
            for(let i=0;i<=6;i++)
            this.t.push(tg[i]);
            break;
        case "blue":
            for(let i=0;i<=6;i++)
            this.t.push(tb[i]);
            break;
        case "yellow":
            for(let i=0;i<=6;i++)
            this.t.push(ty[i]);
            break;

      } 
    this.open=function()
    {
        this.status=1;
        this.id.style.left=this.t[this.track].left+"px";
        this.id.style.top=this.t[this.track].top+"px";
    }
    this.move=function(count)
    {
        if(this.status==0  && face==6)
        {
            this.open();
            sync=1;
        }
        else if(this.status==1)
        {
        this.track+=count;
        this.id.style.left=this.t[this.track].left+"px";
        this.id.style.top=this.t[this.track].top+"px";
        sync=1;
        }

    }
    
}
function mov(a,c)
{   
    if((syncm==1) && (a==turn))
    {
        switch(a)
        {
            case 0:
                red[c].move(face);
                if(face!=6 && sync==1)
                {
                    turn=(turn+1)%4;
                    display.value="Its "+data[turn]+" turn";
                }
                (sync==1)?syncm=0:syncm=1;
                break;
            case 1:
                green[c].move(face);
                if(face!=6 && sync==1)
                {
                    console.log(sync);
                    turn=(turn+1)%4;
                    display.value="Its "+data[turn]+" turn";
                }
                (sync==1)?syncm=0:syncm=1;
                break;
            case 2:
                yellow[c].move(face);
                if(face!=6 && sync==1)
                {
                    turn=(turn+1)%4;
                    display.value="Its "+data[turn]+" turn";
                }
                (sync==1)?syncm=0:syncm=1;
                break;
            case 3:
                blue[c].move(face);
                if(face!=6 && sync==1)
                {
                    turn=(turn+1)%4;
                    display.value="Its "+data[turn]+" turn";
                }
                (sync==1)?syncm=0:syncm=1;
                break;
        }
    }
}
function start()
{
    if(star.value=="start")
    {
        star.value="stop";
        display.value="Its Red Turn";
        turn=0;
        sync=1;
     }   
    else
    {
        document.location.reload();
    }
}
function shuffle()
{
        if(sync==1)
        {
            var i=Math.floor(Math.random() * 6) + 1;
           // i=k.pop();
            dice.src='img/'+i+'.png';  
            face=i;   
            if(check(data[turn])!=1)
            {
  
                if(face==6)
                {
                    sync=0;
                    syncm=1;
                }
                else
                {
                    turn=(turn+1)%4;
                    display.value="Its "+data[turn]+" turn";
                    sync=1;
                }
            }
            else
            {
                    
                    sync=0;
                    syncm=1; 
            }

        }
}

red.push(new obj("red",document.getElementById("r1"),0));
red.push(new obj("red",document.getElementById("r2"),0));
red.push(new obj("red",document.getElementById("r3"),0));
red.push(new obj("red",document.getElementById("r4"),0));

green.push(new obj("green",document.getElementById("g1"),13));
green.push(new obj("green",document.getElementById("g2"),13));
green.push(new obj("green",document.getElementById("g3"),13));
green.push(new obj("green",document.getElementById("g4"),13));

blue.push(new obj("blue",document.getElementById("b1"),39));
blue.push(new obj("blue",document.getElementById("b2"),39));
blue.push(new obj("blue",document.getElementById("b3"),39));
blue.push(new obj("blue",document.getElementById("b4"),39));

yellow.push(new obj("yellow",document.getElementById("y1"),26));
yellow.push(new obj("yellow",document.getElementById("y2"),26));
yellow.push(new obj("yellow",document.getElementById("y3"),26));
yellow.push(new obj("yellow",document.getElementById("y4"),26));
